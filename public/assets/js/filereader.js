//check if browser supports file api and filereader features
if (window.File && window.FileReader && window.FileList && window.Blob) {

	//this is not completely neccesary, just a nice function I found to make the file size format friendlier
	//http://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable
	function humanFileSize(bytes, si) {
		var thresh = si ? 1000 : 1024;
		if (bytes < thresh) return bytes + ' B';
		var units = si ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
		var u = -1;
		do {
			bytes /= thresh;++u;
		} while (bytes >= thresh);
		return bytes.toFixed(1) + ' ' + units[u];
	}

	function render_image(file) {
		var reader = new FileReader();

		reader.onload = function(event) {
			the_url = event.target.result

			$('#preview').html("<img src='" + the_url + "' />")
		}

		//when the file is read it triggers the onload event above.
		reader.readAsDataURL(file);
	}

	function read_vcard(file) {
		var reader = new FileReader();

		reader.onload = function(event) {
			the_blob = event.target.result

			//			console.log(the_blob)
			var contacts_hashed = vcard_process(the_blob);
			console.log(contacts_hashed);
			// here is where you would then upload the contacts
		}

		//when the file is read it triggers the onload event above.
		reader.readAsText(file);
	}

	//this function is called when the input loads an image
	function read_file(file) {

		console.log(file.type)
		console.log(file.name)
		console.log(humanFileSize(file.size, "MB"))

		if (file.type.match(/^image/)) {
			render_image(file);
		} else {
			read_vcard(file);
		}

	}


	//watch for change on the 
	$("#file_read").change(function() {
		console.log("file has been chosen")
		//grab the first image in the fileList
		//in this example we are only loading one file.
		console.log(this.files[0].size)
		read_file(this.files[0])

	});


} else {

	alert('The File APIs are not fully supported in this browser.');

}