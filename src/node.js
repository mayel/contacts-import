var util = require('util');
var vCard = require('vcard-js');

const crypto = require('crypto');

const hash_salt = 'moodlenet_contacts';

// global function to process vcard
window.vcard_process = function(vcard_string) {

	var json = vCard.parse(vcard_string);

	//	console.log(util.inspect(json));
	//	console.log(json)
	contacts_hashed = [];

	for (var i = 0; i < json.length; i++) {
		//		console.log(json[i]);
		//		var contact = json[i]

		for (var j = 0; j < json[i].items.length; j++) {
			//		console.log(json[i].items[j]);
			if (json[i].items[j].name == 'EMAIL' || json[i].items[j].name == 'TEL') {

				var hash = contact_detail_hash(json[i].items[j].value);

				var dbg = "Contact # " + i + ": " + json[i].items[j].value + ": " + hash;
				//			console.log(dbg)

				if (contacts_hashed[i] == null) contacts_hashed[i] = {}
				if (contacts_hashed[i][json[i].items[j].name] == null) contacts_hashed[i][json[i].items[j].name] = []

				contacts_hashed[i][json[i].items[j].name].push(hash);

				$("#preview").append("<li>" + dbg); // for debug only

			}
		}
	}

	//	console.log(contacts_hashed)

	return contacts_hashed;
}

window.contact_detail_hash = function(string) {
	return crypto.createHmac('sha1', hash_salt).update(string).digest('base64');
}