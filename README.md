# Address Book Hashing
A privacy-friendly proof-of-concept that places a user's address book in their browser's memory, and then hashes (with one-way encryption) emails and phone numbers *before* uploading them. This is intended to power the "find my friends" functionality on [MoodleNet](https://moodle.com/moodlenet).

## Screenshot
![Screenshot](samples/screenshot.png)

## Currently supports
* "uploading" vCard files into browser memory and processing into an array
* fetching contacts list from Google API using client-side JavaScript
* hashing each email and phone number (currently using sha1)

## TODO
* support CSV address book files
* normalise data before hashing (lowercase, remove spaces, etc)
* research pros/cons of various hashing algorithms
* upload hashed data to API in the background
* explore mobile support (probably would have to package as app with Cordova)


## Relevant files
* `src/node.js` contains code to process the vcard and make hashes, using Node libs
* `public/assets/js/filereader.js` contains code to "upload" a file into memory using HTML5 functions
* `public/assets/js/google_contacts.js` contains code to fetch contacts from Google's API
* `public/index.html` contains sample UI
* `prep.sh` contains the command to package `src/node.js` into `public/assets/js/node_bundle.js` using browserify

## License
AGPL 3.0+
